---
title: The WKB approximation
---

# The WKB wavefunction

```{python initialize=True}
from IPython.display import HTML, display
import common

common.configure_plotting()
```

!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Write down the Schrödinger equation of a particle in a one-dimensional potential.
    - Solve first order ordinary differential equations.


!!! summary "Learning goals"

    After this lecture you will be able to:

    - Derive the WKB wavefunction for the propagating and evanescent regimes from a intuitive and formal approach.
    - Write down the general solution of Schrödinger equation of a particle in a one-dimensional potential in WKB approximation.
    - Explain and discuss the limits of the assumptions used to derive the WKB wavefunction.


## Premise

In the chapter on the WKB approximation we will focus on a specific problem: namely solving the quantum problem of one particle in a one-dimensional potential $V(x)$, where $x$ is the spatial coordinate in 1D. The stationary Schrödinger equation for the wave function $\psi(x)$ reads in this case

$$-\frac{\hbar^2}{2m} \frac{d}{dx} \psi(x) + V(x) \psi(x) = E \psi(x)\,. \tag{1}\label{eq:schrodinger}$$

Our goal now will be to derive an approximate result for the wave function $\psi(x)$ if the potential $V(x)$ is smooth (where smooth will still have to be properly defined).

In the previous section, we learned that smooth potentials prevent backscattering. Using this intuition we will first derive an approximation for the wave function making use of some heuristic arguments. Afterwards we will justify this rather intuitive derivation by showing that we can get the same result using a formal approach, too.

The approximation for the wave function we are deriving here is typically referred to as Wentzel-Kramer-Brillouin (WKB) approximation, named after the physicists Gregor Wentzel, Hendrik Anthony Kramers und Léon Brillouin.

## Intuitive derivation

To derive the WKB approximation in an intuitive fashion we will use two main assumptions:

* There is no back reflection in a smooth potential.
* A smooth potential can be decomposed into piecewise constant pads. 

We can without restriction rewrite the wavefunction as
$$
\psi(x) = A(x)e^{i\varphi(x)}. \tag{2}\label{eq:ansatz}
$$
where $A(x)$ is the amplitude and $\varphi(x)$ the phase (with both being real).

```python inline
import matplotlib.pyplot as plt
import numpy as np

def discretize(x, y, step):
    z = np.ones_like(y)
    n_points = len(x)
    n_new = int(max(x)/step)
    d = int(n_points/n_new)
    for i in range(0, n_new, 2):
        z[i*d:(i+2)*d] = y[(i+1)*d]
    return z

x = np.linspace(-1, 1, 200)*np.pi/2
x1 = np.linspace(0,3.01,200)
x2 = np.linspace(1,2,100)
y = np.sin(x)
y1 = np.heaviside(x1-1, 0 ) + np.heaviside(x1-2, 0 ) + np.heaviside(x1-3, 0 )
y2 = (5-np.sin(x2*5*np.pi))
y3 = (5-np.sin(x1*5*np.pi))
z1 = discretize(x, y, step=0.1)

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 4))
ax[0].plot(x, y, label='smooth', color='red', linewidth=3, zorder=100)
ax[0].plot(x, z1, label=r'$discrete$', color='black')
ax[0].set_ylabel(r'$V(x)$',fontsize=18)

ax[1].plot(x1, y1, label=r'V(x)', color='black')
ax[1].plot(x2, y2, label=r'$\psi(x)$', color='red')
ax[1].plot(x1, y3, color='red', linestyle='dotted', linewidth=0.5)
ax[1].set_ylim(-0.1, 8.5);
ax[1].vlines(x=[1, 2], ymin=1, ymax=7, linestyle='dashed', color='red')
ax[1].text(0.7, 3.1, r'$\varphi_i$', fontsize=18, color='red')
ax[1].text(2.1, 3.1, r'$\varphi_{i+1}$', fontsize=18, color='red')
ax[1].text(1.4, 6.5, r'$\Delta\varphi$', fontsize=18, color='red')

for axes in ax:
    axes.legend(fontsize=12)
    axes.set_xlabel(r'$x$',fontsize=18)
    axes.set_xticks([]);
    axes.set_yticks([]);
fig.show()
```

First, let us focus on the phase $\varphi(x)$ shown in the right plot above. When  moving along a single potential step, the phase acquires a constant shift proportional to the wavevector and the travelled distance,
$$
\varphi(x_{i+1}) = \varphi(x_{i})+ \Delta \varphi.
$$ 
Here we make use of the fact that there is no backreflection, so that it is sufficient to consider the contribution of a single wave. Over a constant potential, the acquired phase is $\Delta \varphi = p(x_i) \Delta x / \hbar$, where we define the momentum at position $x$ as 
$$
p(x)=\sqrt{2m(E-V(x))} \tag{3}\label{eq:momentum}.
$$
The total phase of the wave function is then obtaining by summing the contributions of all the steps, that is,
$$
\psi(x) \sim \exp\left(\pm \frac{i}{\hbar} \sum_{j=0}^N p(x_j) \Delta x\right) =\exp\left( \frac{\pm i}{\hbar} \int_{x_0}^x p(x') d x'\right).
$$
In the last equality, we take the limit $\Delta x \rightarrow 0$ to convert the summation to an integral.

Having found an expression for $\varphi(x)$, let us consider the amplitude $A(x)$. To this end we note that the solution we have been constructing so far is a travelling wave - remember, we assumed that there is no backreflection! Such a travelling wave has an associated probability current
$$j(x) = \frac{\hbar}{2 m i} \left(\psi(x) \frac{d}{dx} \psi^*(x) - \psi^*(x) \frac{d}{dx} \psi(x)\right)\,. \tag{4}$$
Plugging the ansatz \eqref{eq:ansatz} for the wave function into this expression for the current, we find
$$
j(x) = \frac{\hbar}{m} A^2{x} \frac{d \varphi}{dx} = A^2(x) \frac{p(x)}{m}\,.
$$
From quantum mechanics we know that the probability current is conserved, i.e. $j(x) = j_0 = \text{const}$. We thus solve for $A(x)$ to find
$$
A^2(x) = \frac{m j_0}{p(x)}\quad \Rightarrow \quad A(x) = \sqrt{\frac{m j_0}{p(x)}}.
$$
Typically, the proportionality constants $m$ and $j_0$ are left out and we write
$$
A(x) \sim \frac{1}{\sqrt{p(x)}}.
$$
At first glance this seems weird - now the amplitude doesn't even have the correct units! The reason for doing this is that in most cases it will be sufficient to derive expressions of the wave function up to a global, constant factor as we will see in the explicit examples derived in the next sections. If you however need a properly normalized wave function in your application, remember to normalize it properly at the end.

Combining our results for $\varphi(x)$ and $A(x)$ we then arrive at the wave function in WKB approximation:
$$
\psi_{WKB}(x) \sim \frac{1}{\sqrt{p(x)}} \exp\left( \pm \frac{i}{\hbar} \int_{x_0}^x p(x') d x'\right).
$$

!!! summary "Energy dependence and turning points"

    - Observe that the energy enter as a parameter instead of being an outcome of calculations.
    - When $V(x_0)=E$, the amplitude $A$ of $\psi$ diverges. This means that the WKB approximation is no longer valid, i.e. we do need to restrict ourselves to strictly $E>V(x)$ at this point. Observe that these points are a function of energy, i.e. $x_0 = x_0(E)$.

## Formal derivation
We start again from the Schrödinger equation, and rewrite it using the definition \eqref{eq:momentum} of the momentum $p(x)$:
$$
\begin{align}\label{eq:schrod}
& -\frac{\hbar^2}{2m} \frac{d^2}{dx^2} \psi(x) + V(x) \psi(x) = E\psi(x)\\
\Rightarrow  \quad & \psi''(x) + \frac{p^2(x)}{\hbar^2} \psi(x) = 0.
\end{align}
$$
We consider $E>V(x)$, so $p(x)$ is real. We use again the general wave function ansatz \eqref{eq:ansatz}, i.e. $\psi(x) = A(x) \exp(i\varphi(x))$, where $A$ and $\varphi$ are real. In the following we will suppress the $x$-dependence in the notation and simply write $\psi=A e^{i\varphi}$.

The second spatial derivative of $\psi$ is
$$ 
\psi'' = A''e^{i\varphi}+2i A' \varphi' e^{i\varphi}- A (\varphi')^2 e^{i\varphi} + i A \varphi'' e^{i\varphi}. 
$$
Inserting this expression in the Schrödinger equation and dividing by $e^{i \varphi}$ gives
$$
A'' + 2i A' \varphi' - A ( \varphi' )^2  + i A \varphi''  + \frac{p^2}{\hbar^2} A = 0,
$$
Both the real and imaginary part of this equation need to be zero individually. Since both $A$ and $\varphi$ are real, we obtained two simpler equations:
$$
\begin{align}
A \varphi'' + 2 A' \varphi' &= 0 \tag{5}\label{eq:imagpart}\\
A'' - A ( \varphi' )^2 + \frac{p^2}{\hbar^2} A &= 0 \tag{6}\label{eq:realpart}.
\end{align}
$$
The solution to \eqref{eq:imagpart}can be obtained by multiplying the equation by $\varphi'$. That is,
$$
\begin{align}
 & \varphi'\left(A \varphi'' + 2 A' \varphi'\right) = (A^2 \varphi')' = 0 \\
 \Rightarrow \quad & A = \frac{c}{\sqrt{\varphi'}}.
\end{align}
$$
To solve \eqref{eq:realpart}, let us assume that $A'' \ll A(\varphi')^2$, i.e. we drop $A''$. Then, we find that the equation can be solved as
$$
\begin{align}
& -A (\varphi')^2 + \frac{p^2}{\hbar^2} A = 0\\
\Rightarrow \quad &  (\varphi')^2 = \frac{p^2}{\hbar^2}\\
\Rightarrow \quad &  \varphi' = \pm \frac{p}{\hbar}\\
\Rightarrow \quad & \varphi(x) = \pm \frac{1}{\hbar}\int_{x_0}^x p('x) dx'. 
\end{align}
$$

Then, we can conclude that the WKB wavefunction is given as,
$$
\psi(x) = \frac{1}{\sqrt{p(x)}} \exp\left( \pm \frac{i}{\hbar} \int_{x_0}^x p(x') d x'\right).
$$
For the case $E < V(x)$, we use the same argument as before to find exponential decaying and exponential growing solutions.

!!! summary "Smooth potential approximation"
    
    We have assumed that $A'' \ll A(\varphi')^2$. What does this approximation mean physically?

    We can expect that the amplitude $A$ changes over the length scale over wich $V(x)$ changes. If we define this length scale as $\Delta L$, we can estimate that $A' \sim \frac{1}{\Delta L}$ and $A'' \sim \frac{1}{\Delta L^2}$. On the other hand, $A(\varphi')^2$ is dominated by the change in $\varphi$ that is given by the oscillatory nature of the wave function, and characterized by the wave length $\lambda$. We can thus estimate $A (\varphi')^2 \sim \frac{1}{\lambda^2}$. with this, we find the condition
    $$
    \begin{align}
    & A'' \ll A(\varphi')^2\\
    \Rightarrow \quad & \frac{1}{\Delta L^2} \ll \frac{1}{\lambda^2}\\
    \Rightarrow \quad &  \lambda \ll \Delta L.
    \end{align}
    $$
    This indeed implies that the potential $V(x)$ should change slowly compared to the wave length $\lambda$.

### Evanescent waves: $E < V(x)$

We assumed in our derivation that $E>V(x)$, but it is straightforwardly extended to $E < V(x)$ as well. In this case, the wavefunction does not accumulate a phase, but accumulates a decaying amplitude. On the formal level, when $E< V(x)$
it is useful to write 
$$
p(x)=\sqrt{2m(E-V(x))}=i\sqrt{2m(V(x)-E)}=i|p(x)|.
$$
where $|p(x)|$ is now a positive and real function. The WKB function then is given by
$$
\psi_{WKB}(x) = \frac{1}{\sqrt{|p(x)|}} \exp\left( \frac{\pm 1}{\hbar} \int_{x_0}^x |p(x')| d x'\right).
$$

## Summary

We have derived the wave functions in WKB approximation under the assumption of a slowly changing potential - slow compared to the wave length. Since the Schrödinger equation \eqref{eq:schrodinger} is a second-order differential equation, the general solution will be a linear superposition of the two fundamental solutions ($\pm$) we found above:

$$
\begin{split}
\psi(x)_{E > V(x)} &=  \frac{A}{\sqrt{p(x)}}e^{\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'} + \frac{B}{\sqrt{p(x)}}e^{-\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'},\\
\psi(x)_{E < V(x)} &=  \frac{C}{\sqrt{|p(x)}|}e^{\frac{1}{\hbar} \int_{x_0}^{x} |p(x')| dx'} + \frac{D}{\sqrt{|p(x)|}}e^{-\frac{1}{\hbar} \int_{x_0}^{x} |p(x')|dx'}.
\end{split}
$$
