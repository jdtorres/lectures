# Bound states

```python inline
import common

common.configure_plotting()
```


!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Write down the WKB wave function
    - Apply the connection formulas to connect the WKB wave functions across a turning point
    - Apply boundary conditions to the solution of a differential equation.


!!! summary "Learning goals"

    After this lecture you will be able to:

    - Compute the energy of a bound states using the WKB approximation for three different potential configurations.

## Potential Wells

With the connection formulas, we are now able to apply the WKB approximation to any sufficiently smooth potential $V(x)$!

We will first consider the case of *potential wells*: potentials $V(x)$ that have regions $V(x)>E$ for all $x < x_1$ and $x>x_2$. Or in other words, $V(x)<E$ only for $x_1<x<x_2$:

![potential well](figures/wkb_boundstates.svg)

In such a potential well, we expect that the wave function is bound to the center of the well. From quantum mechanics we know that in such a case the Schrödinger equation has a spectrum of discrete eigenenergies. How can we reconcile this with the WKB approximation, where the energy $E$ was actually an input parameter? Below, we will see that the boundary conditions/connection formulas will give conditions on the allowed values of $E$, resulting in a discrete set of eigenenergies.

In the examples we consider, we have to kinds of boundaries:

- a smooth potential $V(x)$. This leads to a turning point $x_t(E)$ that depends on $E$, with $V(x_t(E)) = E$. We then have to apply the connection formulas at this turning point.
- a hard wall at some point $x_\text{hw}$. This corresponds to demanding that the wave function needs to be zero there: $\psi(x_\text{hw}) = 0$. Away from $x_\text{hw}$ the WKB approximation holds (we assume a smooth potential there), so that we can simply impose this boundary condition on the WKB wave function.

In all the exmples below, we will only be interested in the eigenenergies, and thus do not need to worry about the normalization of the WKB wave function.

### Potential with one hard  wall
![One wall](figures/one_wall.svg)

Conside the case shown above with one hard wall. Observe that in this case the position of the hard wall $x_1$ does not depend on $E$. Only the right turning point is a function of $E$, i.e. $x_2 = x_2(E)$. In order to have a normalizable wave function, $\psi_{WKB}(x)$ can only contain a decaying wave function for $x>x_2(E)$. Consulting the [connection formulas](wkb_connection.md#summary-of-the-connection-formulas) (you may want to open these in a separate tab/window :wink:) we immediately realize that the wave function for $x_1 < x < x_2$ can only contain the contribution
$$
\psi_{WKB}(x) \sim \sin\left( \frac{1}{\hbar}\int_x^{x_2} p(x') dx' + \frac{\pi}{4} \right).
$$
In addition, we need to fulfill the boundary condition $\psi_{WKB}(x_1)=0$. This means that
\begin{align}
&\sin\left( \frac{1}{\hbar}\int_{x_1}^{x_2} p(x') dx' + \frac{\pi}{4} \right) = 0\\
\Rightarrow \quad & \frac{1}{\hbar}\int_{x_1}^{x_2} p(x') dx' + \frac{\pi}{4} = n \pi\\
\Rightarrow \quad & \int_{x_1}^{x_2} p(x') dx' = \left( n - \frac{1}{4} \right)\pi \hbar\quad  \text{for $n=1,2,3,\dots$}.
\end{align}


When deriving the condition above, be aware that since $p(x)>0$ and $x_1<x_2$, we must have $n\geq 1$.

### Potential with no hard walls

![No walls](figures/no_wall.svg)

We now consider a situation with two smooth potential walls. We know that the wave function needs to strictly decay outside $x_1<x<x_2$. Using this fact and the [connection formulas](wkb_connection.md#summary-of-the-connection-formulas) at $x_1$, we know that the wave function for $x_1<x<x_2$ is 
$$
\psi_{WKB}(x) \sim \sin \left( \frac{1}{\hbar} \int_{x_1}^{x} p(x')dx'  + \frac{\pi}{4}\right).
$$
We now also want to make use of the connection formula at $x_2$, but the wave function is not in the right form - we need an integral of the form $\int_x^{x_2}$ inside the argument of the sine! To get there, we do some algebraic manipulations:
\begin{align}
\psi_{WKB}(x) &\sim \sin \left( \frac{i}{\hbar} \int_{x_1}^{x} p(x')dx'  + \frac{\pi}{4}\right)\\
& = \sin \left( \frac{1}{\hbar} \int_{x_1}^{x_2} p(x')dx'  - \frac{1}{\hbar} \int_x^{x_2}p(x')dx' + \frac{\pi}{4}\right)\\
&= \sin \left( \frac{1}{\hbar} \int_{x_1}^{x_2} p(x')dx'  +\frac{\pi}{2} - \left[\frac{1}{\hbar} \int_x^{x_2} p(x')dx'+ \frac{\pi}{4}\right]\right)
\end{align}
Making use of the trigonometric identity $\sin(\alpha-\beta) = \sin(\alpha) \cos(\beta) -\cos(\alpha) \sin(\beta)$ we thus can write
\begin{align}
\psi_{WKB}(x) \sim &\,\,\sin\left(\frac{1}{\hbar} \int_{x_1}^{x_2} p(x')dx'  +\frac{\pi}{2}\right) \cos\left(\int_x^{x_2} p(x')dx'+ \frac{\pi}{4}\right)\\
&-\cos\left(\frac{1}{\hbar} \int_{x_1}^{x_2} p(x')dx'  +\frac{\pi}{2}\right) \sin\left(\int_x^{x_2} p(x')dx'+ \frac{\pi}{4}\right)\,.
\end{align}
Since the wave function for $x>x_2$ must strictly decay, we can deduce from the [connection formulas](wkb_connection.md#summary-of-the-connection-formulas) that only the second term of this wave function mahy be present. Hence, we find the condition for a valid solution
\begin{align}
&\sin\left(\frac{1}{\hbar} \int_{x_1}^{x_2} p(x')dx'  +\frac{\pi}{2}\right) = 0\\
\Rightarrow \quad & \frac{1}{\hbar} \int_{x_1}^{x_2} p(x')dx'  +\frac{\pi}{2} = n\pi\\
\Rightarrow \quad & \int_{x_1}^{x_2} p(x') dx' = \left( n - \frac{1}{2} \right)\pi \hbar\quad \text{for n=1,2,3,\dots}.
\end{align}


### Potential with two vertical walls

![Two walls](figures/two_wall.svg)

Finally, consider the case of a quantum well with vertical walls. In this case, none of the turning points depend on energy. Since there are no smooth potentials, we do not need the connection formulas in this case. This also means there is no need to use the $\sin$ and $\cos$ basis used in the connection formulas, and we can write the wavefunction inside the hard walls as,
$$
\psi_{WKB}(x) \sim \frac{1}{\sqrt{p(x)}} \left( A e^{\frac{i}{\hbar} \int_{x_1}^x p(x')dx'} + B e^{-\frac{i}{\hbar} \int_{x_1}^x p(x')dx'} \right).
$$
We apply the boundary condition $\psi_{WKB}(x_1)=0$ and find that,
$$
A = - B  \rightarrow \psi_{WKB}(x) \sim \frac{1}{\sqrt{p(x)}} \sin \left( \frac{i}{\hbar} \int_{x_1}^x p(x')dx' \right).
$$
Then, we apply the second boundary condition $\psi_{WKB}(x_2)=0$, and we find
$$
\sin \left( \frac{i}{\hbar} \int_{x_1}^{x_2} p(x')dx' \right) = 0.
$$
So we conclude,
$$
\int_{x_1}^{x_2} p(x') dx' = n\pi \hbar
$$

## Summary

```python inline
import matplotlib.pyplot as plt
import numpy as np

x0 = np.linspace(-np.pi, np.pi, 100)
x1 = np.linspace(0, 2, 100)
y0 = -np.cos(x0)
y1 = x1**2
y1[0] = 4
y2 = np.sin(-x0)/4
y2[0] = 3
y2[-1] = 3

fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(12, 5))
axes[0].plot(x0, y0, label=r'$V(x)$', color='black', linewidth=1)
axes[0].axhline(0, color='r', label=r'$E$')
axes[0].set_title(r'$\nu=1/2$', fontsize=18)
turning0 = np.pi/2*np.array([-1, 1])
axes[0].scatter(turning0, turning0*0, color='black', zorder=10)
axes[0].vlines(x=turning0, ymin=-2, ymax=0, color='black', linestyle='dashed')
axes[0].set_ylim(-1.1, 1.1);

axes[1].plot(x1, y1, label=r'$V(x)$', color='black', linewidth=1)
axes[1].axhline(2, color='r', label=r'$E$')
axes[1].set_title(r'$\nu=1/4$', fontsize=18)
axes[1].scatter(np.sqrt(2), 2, color='black', zorder=10)
axes[1].vlines(x=np.sqrt(2), ymin=-2, ymax=2, color='black', linestyle='dashed')
axes[1].set_ylim(-0.1, 4);

axes[2].plot(x0, y2, label=r'$V(x)$', color='black', linewidth=1)
axes[2].axhline(2, color='r', label=r'$E$')
axes[2].set_title(r'$\nu=0$', fontsize=18)
xt = np.array([-np.pi/2, np.pi/2])
for ax in axes:
    ax.set_xticks([]);
    ax.set_yticks([]);
    ax.set_xlabel(r'$x$', fontsize=15)
plt.legend(prop={'size': 14})
fig.show()
```

The three cases depicted here can be summarized in the following formula for bound state energies
$$
\int_{x_1}^{x_2} p(x') dx' = (n-\nu)\pi \hbar.
$$
Here $\nu=0, \frac{1}{2}, \frac{1}{4}$ for the different potential wells.

!!! warning "Where is the energy?"
    Note that depending on the type of potential well, the energy $E$ enters in different places:

    - The integrand $p(x') = \sqrt{2m(E-V(x))}$ depends explicitly on $E$.
    - The integral bounds $x_1$ and $x_2$ depend on $E$ if the respective boundary is formed by a smooth potential.

    Solving the integral for a specific problem then leads to an equation that still needs to be solved for $E$ to obtain an explicit expression for the eigenenergies.
