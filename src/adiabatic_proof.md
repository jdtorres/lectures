# Proof of adiabatic theorem

!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Write down the Schrodinger equation.
    - Distinguish between time-dependent and time-independent Schrodinger equation.
    - Solve first order ordinary differential equations.


!!! summary "Learning goals"

    After this lecture you will be able to:

    - Write down the wavefunction of a system under adiabatic changes.
    - Explain the origin of the dynamic and geometrical phases.
    - Write down the criteria required for the adiabatic theorem to hold.

## Proof of the theorem

So far we have just *stated* the theorem and shown a couple examples.
Now we will formulate the theorem in a mathematical way and prove it.

Our starting point is the *time-dependent Schrodinger equation*:
$$
i \hbar \frac{\partial}{\partial t} \left|\Psi(t)\right> = H(t) \left|\Psi(t)\right>\, 
$$
with a time-dependent Hamiltonian $H(t)$. For notational simplicity, we will
in the following often leave out the explicit bra-ket notation, and simple write $\Psi(t)$ instead
of $\left|\Psi(t)\right>$.

We now want to consider a Hamiltonian $H(t)$ that changes slowly in time. But what does *slowly* mean,
slow compared to what? In contrast to the WKB approximation this is less obvious, and
we will find a proper criterion in the course of the proof.

If we have a time-independent Hamiltonian $H$, the time-dependent Schrodinger equation
is readily solved as,
$$
H \phi_n = E_n \phi_n,\quad \phi_n(t)= \phi_n(0) e^{-i E_n t / \hbar},
$$
where we have used $\phi$ to denote the eigenstates of $H$.

To solve the time-dependent problem, we will now start with a definition: We define $\psi_n(t)$ to be those eigenfunctions that solve the
equation
$$
H(t) \psi_n(t) = E_n(t) \psi_n(t)\,
$$
where we now consider time $t$ as a *parameter* of the Hamiltonian that we can fix to some arbitrary value.

!!! warning "Instantaneous eigenstates"

    The states $\psi_n(t)$ *do not solve the time-dependent Schrodinger equation!* (despite the occurance of $H(t)$ in the equation). In contrast, the $\psi_n(t)$ that we have just defined solve a stationary Schrodinger equation (we fix $t$ to some value). As a result they must constitute a complete, orthogonal set for any time $t$. That is: $\langle{\psi_n(t) | \psi_m(t)} \rangle = \delta_{nm}$.

    If we take two different times $t$ and $t'$, we cannot say anything about the inner product $\langle {\psi_n(t) | \psi_m(t')} \rangle$ in general, since the $\psi_m$ could evolve in any way in principle, such that the overlap between $\psi_n$ and $\psi_m$ is no longer 0.


Because the $\psi_n(t)$ form a complete orthonormal set, we can express the the full quantum state $\Psi(t)$ as a linear combination of the states $\psi_n(t)$:

$$
\Psi(t) = \sum_n c_n(t) \psi_n(t) e^{i \theta_n(t)}.
$$

where $\theta_n(t) = -\frac{1}{\hbar} \int_0^t E_n(t) dt$. What we do here is expand the wave function $\Psi(t)$ in a different basis for every value time $t$ (using the set of $\{\psi_n(t)\}$). This is allowed, for a given value of time $t$ the wave function $\Psi(t)$ can be expanded in any arbitrary basis set.

We could have absorbed the exponential factor involving $\theta$ into the $c_n(t)$, but as we will see it is convenient to include it explicitly. This term may look strange, but in fact it is a straightforward generalisation of the phase factor that an eigenstate picks up, which would be $\exp (-i E_n t/ \hbar)$,
when using a constant Hamiltonian.

Now we have an expression for $\Psi(t)$ and we want to see what that gives us when we apply the time-dependent Schrodinger equation to it. We get
$$
i \hbar \sum_n \left[ \dot{c}_n \psi_n + c_n \dot{\psi}_n +
c_n \psi_n i \dot{\theta}_n \right] e^{i \theta_n} =
\sum_n c_n(t) H(t) \psi_n(t) e^{i \theta_n(t)}.
$$
Now we remember that we defined $\psi_n$ to satisfy $H \psi_n = E_n \psi_n$, and
we recognize that

$$
i \dot{\theta}_n = -\frac{i}{\hbar} E_n(t)
$$

so the $\theta$ derivative term on the left-hand side cancels with the right-hand side of the previous equation. What we are left with is

$$
i \hbar \sum_n \left( \dot{c}_n \psi_n + c_n \dot{\psi}_n \right) e^{i \theta_n} = 0 \\
\sum_n \dot{c}_n \psi_n e^{i \theta_n} = - \sum_n c_n \dot{\psi}_n e^{i \theta_n}
$$

By projecting these expressions on the $\psi_m$ eigenstate (which amounts to applying $\langle\psi_m|$ from the left on both sides), we find that

$$
\dot{c}_m = - \sum_n c_n \langle{\psi_m| \dot\psi_n} \rangle e^{i (\theta_n - \theta_m)}
$$
where we used the orthogonality of $\psi_n$ and $\psi_m$ to eliminate the sum on the left-hand side.

In order to make progress evaluating what $\langle{\psi_m| \dot\psi_n} \rangle$ might be, we take the derivative of the time-independent Schrodinger equation which we used to define the $\psi_n$ in the first place. That is,

$$
\dot{H}\psi_n + H \dot{\psi}_n = \dot{E}_n \psi_n + E_n \dot{\psi}_n
$$

Again projecting this onto the $\psi_m$ gives us:

$$
\begin{aligned}
\langle{\psi_m | \dot{H} | \psi_n }\rangle + \langle{\psi_m | {H} | \dot \psi_n }\rangle =
\dot{E}_n \langle{\psi_m| \psi_n} \rangle + E_n \langle{\psi_m| \dot\psi_n} \rangle \\
\langle{\psi_m | \dot{H} | \psi_n }\rangle + E_m\langle{\psi_m| \dot\psi_n} \rangle =
\dot{E}_n \delta_{nm} + E_n \langle{\psi_m| \dot\psi_n} \rangle \\
\end{aligned}
$$

So for $n\neq m$, we find that

$$
\langle{\psi_m| \dot\psi_n} \rangle = \frac{\langle{\psi_m | \dot{H} | \psi_n }\rangle}{E_n - E_m}.
$$

When we have slow, gradual change being applied to the system, $\dot{H}$ is small. Now we are finally ready to make the **approximation** that you must have been anticipating since reading the title of these notes. We *neglect* the contributions when $n \neq m$ and just get:

$$
\dot{c}_m = -c_m \langle{\psi_m| \dot\psi_m} \rangle.
$$

!!! summary "Adiabatic approximation"

    What is considered *slow* is determined by $E_n - E_m$. If the energy difference is *large*, then a gradual change could mean something that is much faster than we might expect. We also see that in cases of degeneracy  (when $E_n = E_m$ despite $n \neq m$), there isn't *any* definition of slow that is slow enough. The adiabatic approximation breaks down here. Similarly if we change the system in such a way that two energy levels that were separated come together or switch places, the approximation will again break down.

Solving the equation for the coefficients gives

$$
c_m(t) = c_m(0) e^{i \gamma(t)}, \quad \text{ where  } \gamma(t) = i \int_0^t \langle{\psi_m| \dot\psi_n} \rangle dt'
$$

(You may notice that the factor $i$ appears twice in the above expression, both in the expression $e^{i \gamma(t)}$ and in the definition of $\gamma(t)$. This is not a mistake, as we will see later.)

So if we start in the $n^{\text{th}}$ eigenstate of the initial Hamiltonian,
that means that $\Psi = \psi_n$. So $c_n(0) = 1$ and all $c_m(0) = 0$ for $n
\neq m$. Then the full wavefunction after the gradual change has taken place is

$$
\Psi(t) = e^{i \theta_n(t)} e^{i\gamma_n(t)} \psi_n(t).
$$
The $n^{\text{th}}$ eigenstate stays in the $n^{\text{th}}$ eigenstate.

### Adiabatic criterium: how slow is *slow*?

In order to obtain the result above, the terms $n\neq m$ have been neglected by considering their contribution small, arguing that there should be a *large enough* energy gap between the instantaneous eigenenergies. Let us now derive a more quantitative criterion.

To derive it, we will proceed in the spirit of perturbation theory: we start with the initial approximation and then insert it back to get a higher-order approximation. In our [derivation for the adiabatic theorem](adiabatic_proof.md##proof-of-the-theorem) we find as an intermediate result
$$
\begin{align}
\dot{c}_{n} &= - \sum_k c_k \langle{\psi_n| \dot\psi_k} \rangle e^{i (\theta_k - \theta_n)}\\
&= \sum_k c_k \frac{\langle \psi_n | \dot H |\psi_k \rangle}{E_n(t) - E_k(t)} e^{-\frac{i}{\hbar} \int_{0}^t E_k(t') - E_n(t') dt'}.
\end{align}
$$
We then found the approximate solution $c_m(t) = 1$ and $c_{n\neq m}(t)=0$. Let us now put this approximate solution back into the still exact equation to find
$$
\dot c_{n \neq m} = \frac{\langle \psi_n | \dot H |\psi_m \rangle}{E_n(t) - E_m(t)} e^{-\frac{i}{\hbar} \int_{0}^t E_m(t') - E_n(t') dt'}\,.
$$
Integrating this equation we then arrive at
$$
\rightarrow c_{n \neq m}(T) = \int_{0}^T \frac{\langle \psi_n | \dot H |\psi_m\rangle}{E_n(t) - E_m(t)} e^{\frac{i}{\hbar} \int_{0}^t E_n(t') - E_m(t') dt'} dt.
$$
Of course we know that $c_{n \neq m}(T)$ should be zero - that was what we input into the equation to begin with! We will now be able to find our quantitative criterion for adiabaticity by finding out under whch conditions $c_{n \neq m}(T)$ will be very small.

There are several time-dependent quantities in the previous expression. To find a solution, we will  approximate some of them by using the following constant bounds: 

* The largest contribution from the rate of change in the Hamiltonian will come from the largest matrix element

$$
\langle \psi_n (t) | \dot H |\psi_m(t) \rangle \approx \overline{\langle \psi_n | \dot H |\psi_n\rangle}
$$

* The smallest energy difference will contribute the most.

$$
E_n(t) - E_m(t) \approx \overline{E_n - E_m}
$$

Replacing these time-dependent quantities with these extremal bounds, we arrive at a simpler problem that we can now solve analytically:
$$
\begin{align}
c_{n\neq m}(T) &\approx \int _0^T \frac{\overline{\langle \psi_n | \dot H |\psi_m\rangle}}{\overline{E_n - E_m}} e^{\frac{i}{\hbar} \int_0^t \overline{E_n - E_m} dt'} dt\\
&=\frac{\overline{\langle \psi_n | \dot H |\psi_m\rangle}}{\overline{E_n - E_m}} \int_0^T e^{\frac{i}{\hbar} \overline{E_n - E_m} t} dt\\
&= \frac{\overline{\langle \psi_n | \dot H |\psi_m\rangle}}{\overline{E_n - E_m}} \frac{i\hbar }{\overline{E_n - E_m}}\left( e^{-i \overline{E_n - E_m} T /\hbar } - 1 \right)\\
&\approx \frac{\overline{\langle \psi_n | \dot H |\psi_m\rangle}}{\overline{E_n - E_m}} \frac{i\hbar }{\overline{E_n - E_m}}
\end{align}
$$
In the last line we made use of the fact that the last term of the previous line is oscillating. Since we are only interested in upper bounds, we can approximate it with a constant 1. To get our adiabaticity criterion, we now remember that we need to have $|c_{n\neq m}(T)| \ll 1$, so that
$$
 \frac{\hbar \overline{\langle \psi_m | \dot H |\psi_n\rangle} }{\overline{E_n - E_m}^2} \ll 1
$$
or in other words
$$
 \frac{\overline{\langle \psi_m | \dot H |\psi_n\rangle} }{\overline{E_n - E_m}} \ll \frac{\overline{E_n - E_m}}{\hbar}\,.
$$
This is a quantitative estimate for the adiabaticity criterion.

## Summary

Under adiabatic evolution, the wavefunction of the system is given as,
$$
\Psi(t) = e^{i \theta_n(t)} e^{i\gamma_n(t)} \psi_n(t).
$$
This means that: 

* The system remains in the instantaneous eigenstate.
* It acquires two phases shifts depending on the evolution. The dynamical phase, $\theta_n(t)$, and the geometrical phase $\gamma_n(t)$. They are given as,

$$
\theta_n(t) = i \int_0^t E_n(t') \rangle dt', \quad \gamma_n(t) = i \int_0^t \langle{\psi_n| \dot\psi_n} \rangle dt'.
$$
In order for this description to hold, we require that the system evolves slowly enough. That is, the following condition must be satisfied at all times:
$$
\frac{\hbar \overline{\langle \psi_n | \dot H |\psi_m\rangle} }{\overline{E_n - E_m}^2} << 1, \quad n \neq m.
$$
Here, the overline indicates the largest matrix element, and the smallest energy difference.