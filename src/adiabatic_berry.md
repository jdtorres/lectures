# The Berry Phase

!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Write down the wavefunction after adiabatic evolution.
    - Express a magnetic field $\mathbf{B}$ in terms of the vector potential $\mathbf{A}$.
    - Flip limits of integration.


!!! summary "Learning goals"

    After this lecture you will be able to:

    - Write down the Berry phase as a function of an arbitrary parameter.
    - Describe how a relative phase can be measured.
    - Write down the phase acquired when an electron travels around a flux of magnetic field.
    - Relate the Berry phase to the Ahronov-Bohm effect.

## The Berry Phase
In the previous section, we found that the wavefunction acquires two phases during adiabatic evolution. In particular, the geometrical phase is,

$$
\gamma(t) = i \int_0^t \langle{\psi_m| \dot\psi_n} \rangle dt'.
$$

It is known generally as the Berry phase. Perhaps the first question that comes to mind about it is whether it is a phase at all. We can show that it is, but showing that $\gamma(t)$ is in fact *real*.

We know that $\langle{\psi_n | \psi_n} \rangle = 1$. From there we look at

$$
\frac{d}{dt} \left(  \langle{\psi_n}\rangle \right) =
\langle{\dot \psi_n | \psi_n} \rangle + \langle{ \psi_n | \dot\psi_n} \rangle =
\left(\langle{ \psi_n | \dot\psi_n} \rangle\right)^* + \langle{ \psi_n | \dot\psi_n} \rangle =
2 \Re \left( \langle{ \psi_n | \dot\psi_n} \rangle \right) = 0
$$

So the real part of $\langle{ \psi_n | \dot\psi_n} \rangle$ is 0, meaning that the Berry
phase is real. Typically, $H(t)$ gets its time dependence through some parameter
which depends on $t$, for example on the width of the infinite square well. In
that case we express $H$ as $H(\vec{R}(t))$ where $\vec{R}$ is just the set of
parameters that depend on $t$. Then

$$
\dot{\psi}_n = (\vec{\nabla}_{R} \psi_n) \cdot \dot{R}
$$
and so

$$
\begin{aligned}
\gamma_n(t) &= i \int_0^t \langle{ \psi_n | \vec{\nabla_R}\psi_n} \rangle  \cdot
\dot{R}(t') dt' \\
&= i \int_{\vec{R}(0)}^{\vec{R}(t)} \langle{ \psi_n | \vec{\nabla_R}\psi_n} \rangle
\cdot d\vec{R}.
\end{aligned}
$$

This is a line integral through parameter space which is independent of time.
The Berry phase introduced is independent of how fast the path from $\vec{R}(0)$
to $\vec{R}(t)$ is taken. This is in contrast to $\theta_n(t)$ which has
explicit time dependence, where the more time you take the more phase you
accumulate.

If we consider the case where $\vec{R}(0) = \vec{R}(t)$, then for only one
parameter the Berry phase will always be zero. A non-zero Berry phase arises
only for more than one time-dependent parameter. In the case of more than one
parameter then, the value of the integral depends only on the number of poles
enclosed by the path, discretising the value of $\gamma_n$, and yielding a
result that is path independent.

## Measuring a Phase?

![Splitting mirrors.](figures/2paths.svg)

How can we measure a phase? It may seem difficult since phases often don't contribute to observables that we are interested in measuring. Though it may be impossible to measure an *overall* phase, *relative* phases are
fair game. By splitting a wave packet over two paths of different lengths and using interference, we can get information about this phase.

To see this, consider the two paths depicted in the figure above. The longer path is associated with an accumulated phase of $e^{i \gamma}$ for some $\gamma$. The total wavefunction is then

$$
\begin{aligned}
\psi_{tot} &= \frac{1}{2} \psi_0 + \frac{1}{2} \psi_0 e^{i \gamma} \\
|\psi_{tot}|^2 &= \frac{1}{4} |\psi_0|^2 \left( 1 + e^{i \gamma} \right)
\left( 1 + e^{-i \gamma} \right) \\
&= \frac{1}{2} |\psi_0|^2 (1 + \cos(\gamma)) \\
&= |\psi_0|^2 \cos(\gamma/2)
\end{aligned}
$$

We see that relative phases do show up non-trivially when going from amplitudes to probabilities, and exploiting these will be our method toward measuring the Berry phase.

## Aharonov-Bohm Effect

![solenoid](figures/solenoid.svg)

First predicted by Ehrenberg and Siday (10 years earlier than Aharonov and Bohm!), this effect is due to the coupling of the electromagnetic potential to an electron's wave function, as we will see.

The setup is depicted in the figure above. The important thing to note here is that the magnetic field $\mathbf{B}$ is non-zero only inside the solenoid. Which means for both electron paths, through *B* or *C*, the magnetic field is constantly zero. The same cannot be said of the vector potential however.

We know that the flux $\phi$ through the solenoid is $\phi=B\cdot \pi r^2$ for $r$ the radius of the solenoid, and that $\mathbf{B} = \nabla \times \mathbf{A}$.

The vector potential $\mathbf{A}$ changes our Hamiltonian to

$$
H = \frac{(\mathbf{p} + e\mathbf{A})^2}{2m} + V(\mathbf{r}).
$$

If you are wondering about this change, the main point is that the momentum operator has changed in the standard way for quantum mechanics to include the vector potential such that the canonical commutation relation $[r_i, p_j] = i\hbar \delta_{ij}$ still holds.

How now do we solve for $\psi$ in $H \psi = E \psi$ for this Hamiltonian? There
is a simple way to solve this using the solution when $\mathbf{A}=0$, which we
denote as $\psi_0$.

We define

$$
\psi (\mathbf{r}) = e^{i g(\mathbf{r})} \psi_0(\mathbf{r}) ,\text{ where  }
g(\mathbf{r}) = -\frac{e}{\hbar} \int_{\mathbf{r}_0}^{\mathbf{r}}
\mathbf{A}(\mathbf{r}) \cdot d \mathbf{r}
$$

$g(\mathbf{r})$ is defined as a line integral, which is only well-defined if $\mathbf{B} = \nabla \times \mathbf{A} = 0$. This is precisely the situation we have engineered with the solenoid.

It is simple to check that this solution works:

$$
\begin{aligned}
(\mathbf{p} + e\mathbf{A})\psi &= (-i\hbar\nabla + e\mathbf{A})\psi \\
&= -i\hbar e^{i g} i (\nabla g) \psi_0 - i\hbar e^{ig} \nabla \psi_0
+ e \mathbf{A} e^{ig}\psi_0 \\
&= -i\hbar e^{i g} i (-\frac{eA}{\hbar}) \psi_0 - i\hbar e^{ig} \nabla \psi_0
+ e \mathbf{A} e^{ig}\psi_0 \\
&= e^{ig}(\mathbf{p}\psi_0)
\end{aligned}
$$

Applying this operator twice then will give $e^{ig} (\mathbf{p}^2 \psi_0)$. And so since $\psi_0$ satisfies $H_0 \psi_0= E_0 \psi_0$, the function $\psi=e^{ig} \psi_0$ satisfies the equivalent equation with $\mathbf{A}$ in it.

Turning back to the beam splitter and the solenoid, we can see that each path will pick up a different phase factor.

$$
\psi_0 e^{ig} \rightarrow \psi_0 e^{\pm i \gamma} , \text{ where  }
\gamma = -\frac{e}{\hbar} \int \mathbf{A}(\mathbf{r}) \cdot d \mathbf{r}
$$

If, contrary to the figure, we take two semi-circular paths around the solenoid, we can evaluate what the phase associated with each path is using the circle element on the path $r d\phi$.

$$
\gamma = -\frac{e}{\hbar} \int \frac{\phi}{2 \pi r} \hat{\phi} \cdot r d\phi \hat{\phi} =
-\frac{e \phi}{ 2 \pi \hbar} \int d\phi = \pm \frac{e \phi}{2\hbar}.
$$

The *difference* between the two paths is then $e \phi/\hbar$. As we saw in the beam splitter, this difference can be measured in experiment through interference.

Some comments:

* $\mathbf{A}$ matters for this derivation, *not* the magnetic field
  $\mathbf{B}$!
* $\gamma$ is explicitly gauge invariant (adding some term $\nabla f$ to
  $\mathbf{A}$ doesn't change the result)

### Connection to the Berry Phase

If we confine the electron to a box at some point $\mathbf{R}(t)$, and slowly
move the box around the solenoid, we want to find the Berry phase that this
electron acquires as the box moves around the solenoid.

If we had no solenoid, the wavefunction that we would have would be centred
around $\mathbf{R}$, and going around in a circle. This can be written as
$\psi_0(\mathbf{r}-\mathbf{R})$.

With the solenoid present we use the same trick as we did above by setting
$\psi$ to

$$
\psi (\mathbf{r}) = e^{i g(\mathbf{r})} \psi_0(\mathbf{r} - \mathbf{R}), \text{  where  }
g(\mathbf{r}) = -\frac{e}{\hbar} \int_{\mathbf{R}}^{\mathbf{r}} \mathbf{A} \cdot d \mathbf{r}
$$

Then,
$$
\nabla_{\mathbf{R}} \psi(\mathbf{r}) = -i \frac{e}{\hbar} \mathbf{A}(\mathbf{R})
e^{i g(\mathbf{r})} \psi_0(\mathbf{r} - \mathbf{R})
- e^{i g(\mathbf{r})} \nabla_{\mathbf{r}} \psi_0(\mathbf{r} - \mathbf{R})
$$
where we used the fact that $\nabla_{\mathbf{R}} \psi_0(\mathbf{r} -
\mathbf{R}) = -\nabla_{\mathbf{r}} \psi_0(\mathbf{r} - \mathbf{R})$.

We can now use this to evaluate inner product inside the integral of the Berry
phase:

$$
\begin{aligned}
\langle \psi| \nabla_{\mathbf{R}} \psi \rangle  &= \int e^{-ig} \psi_0(\mathbf{r} -
\mathbf{R}) \left[ -\frac{e}{\hbar}\mathbf{A}(\mathbf{R})
    e^{ig}\psi_0(\mathbf{r} - \mathbf{R}) - e^{ig} \nabla_{\mathbf{r}}
    \psi_0(\mathbf{r} - \mathbf{R})\right] d^3r \\
&= -\frac{ie}{\hbar} \mathbf{A}(\mathbf{R}) - \frac{i}{\hbar} \langle{\mathbf{p}}{\psi_0}\rangle.
\end{aligned}
$$

But the expectation value of the momentum is 0 for a particle confined in a box!
So we find that

$$
\begin{aligned}
\gamma_n &= - i \oint \frac{ie}{\hbar} \mathbf{A}(\mathbf{R}) \cdot
d\mathbf{R} \\
&= \frac{e}{\hbar} \int \nabla \times \mathbf{A} da \\
&= \frac{e\phi}{\hbar}.
\end{aligned}
$$

## Summary

The Berry phase can be expressed in terms of an arbitrary time-dependent parameter $\mathbf{R}(t)$ as,
$$
\gamma_n(t) = i \int_{\vec{R}(0)}^{\vec{R}(t)} \langle{ \psi_n | \vec{\nabla_R}\psi_n} \rangle
\cdot d\vec{R}.
$$
The Aharonov-Bohm effect arises as an extra phase due to the coupling of the wavefunction with the vector potential when traveling around a solenoid. The acquired phase is proportional to the magnetic flux going through the solenoid. That is,
$$
\gamma = \pm \frac{e \phi}{2\hbar}.
$$
