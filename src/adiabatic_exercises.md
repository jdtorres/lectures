1.  **Expanding quantum well**

    Consider a quantum well with hard wall boundaries with a width that
    changes in time, $W=W(t)$. One of the walls moves with constant
    velocity $v$ such that the well width changes from $W_1 = W(0)$ to
    $W_2 = W(T)$.

    -   Compute the dynamical phase of this process.

    -   Compute explicitly the geometrical phase of the process (we know
        it has to be zero), by evaluating
        $$\int d\mathbf{R} \langle \psi | \nabla_\mathbf{R} \psi \rangle\,.$$

        *Note: in this general formula, $\mathbf{R}$ stands for the changing
        parameters. What is $\mathbf{R}$ in our case? When evaluating
        the derivative, don't forget the normalization of the wave
        function.*

    -   The dynamical phase depends on the details of the
        time-evolution. Show an explicit example demonstrating that if
        the well width changes in a different manner from $W_1$ to
        $W_2$, the dynamical phase is different.

2.  **Abrupt expansion of a quantum well**

    Consider a particle in a one-dimensional, square quantum well of
    width $W$ with infinitely high walls:

    \begin{equation}
    V(x)=\begin{cases} 0&\text{if $0< x <W$}\\
    	   \infty&\text{else.}\end{cases}
    \end{equation}	

    The particle is initially in the first excited state.
    Then, at time $t=0$ the right wall is suddenly moved from $x=W$ to
    $x=2W$.

    1.  Compute the time evolution of the wave function for $t>0$. To
        this end, expand the wave function at time $t=0$ (for which
        $\psi(x, t=0)$ is the first excited state of the original
        quantum well for $0<x<W$, and $\psi(x, t=0)=0$ for $W<x<2W$) in
        terms of the eigenfunctions of the quantum well with width $2W$.
        Use these to express the time-evolution involving a sum over
        these eigenstates.

    2.  Evaluate the time evolution numerically, and make a few plots
        showing the wave function at different times.

3.  **Recovering the adiabatic theorem for an exactly solvable problem**

    The case of an infinite square well whose right wall expands at a
    constant velocity $v$ can be solved exactly. A complete set of
    solutions is given by (you do *not* need to check this)
    $$\phi_n(x,t) = \sqrt{\frac{2}{W(t)}}
    \sin \left(\frac{n \pi}{W(t)} x \right)
    e^{i(m v x^2 - 2 E_n^0 a t)/2 \hbar W(t)}\,,$$ where
    $W(t) = a + v t$ is the increasing width of the well, and
    $E_n^0= n^2 \pi^2 \hbar^2/2 m a^2$ is the $n$-th eigenenergy of the
    original well with width $a$. The $\phi_n(x, t)$ form a complete and
    orthonormal set at any time $t$. Hence, one can write the general
    time evolution as $$\Psi(x, t) = \sum c_n \phi_n(x,t)\,,$$ with
    time-independent coefficients $c_n$.

    1.  Suppose a particle starts out at $t=0$ in the ground state of
        the quantum well,
        $$\Psi(x, t=0) = \sqrt{\frac{2}{a}} \sin \left(\frac{\pi}{a} x\right)\,.$$
        Show that the expansion coefficients can be written as
        $$c_n = \frac{2}{\pi} \int_0^\pi e^{i \alpha z^2} \sin(n z) \sin(z) dz\,,$$
        where $\alpha = m v a/2 \pi^2 \hbar$.

    2.  Show that in the limit $\alpha \ll 1$ (i.e.
        $e^{i\alpha z} \approx 1$) you recover the result of the
        adiabatic theorem that the system stays in the ground state.
        Rephrase the condition $\alpha \ll 1$ in terms of time scales:
        On the one hand, there is the time to expand the well by length
        $a$, $T_e = a/v$. To which other time do I have to compare to?

    3.  Compare the wave function that you get from a) and b) with the
        wave function from the adiabatic theorem (including phases)!
        From this comparison, what is the value of the geometric phase
        $\gamma(t)$ that you find?

4.  **Berry phase of spin-$\frac{1}{2}$**

    Consider a spin $\frac{1}{2}$ particle in an external magnetic
    field. The magnetic field is rotated slowly, so that the spin always
    points in the direction of the magnetic field.

    In the lecture we showed that the Berry phase over a closed path
    can be expressed as
    $$\gamma(T) = i \oint \langle \psi(t)|
    \nabla_\mathbf{R} \psi\rangle d\mathbf{R}\,.$$
    Using Stoke's theorem the Berry phase can be expressed as
    $$\gamma(T) = i \int \nabla_\mathbf{R} \times \langle \psi |\nabla_\mathbf{R} \psi
    \rangle d\mathbf{a}\,,$$ where the integration is over an area that is bounded
    by the closed path.

    Use this expression to show that the Berry phase accumulated along a
    closed path is given as $$\gamma(T) = -\frac{1}{2} \Omega\,$$ where
    $\Omega$ is the solid angle covered by the rotating field.

    Hints:

    -   The spin-$\frac{1}{2}$ spinor is given as $$\psi=\begin{pmatrix}
        \cos(\theta/2)\\
        e^{i \phi}\sin(\theta/2)
        \end{pmatrix}$$

    -   The set of parameters $\mathbf{R}$ here is $\theta$ and $\phi$.
        For the calculation, it is advantageous to consider this as the
        set of spherical coordinates (we keep the radius $r$ fixed).

        Then it is easiest if you use the expressions for the gradient
        and the crossproduct in spherical coordinates:
        $$\nabla f = \frac{\partial f}{\partial r} \mathbf{e}_r + \frac{1}{r}
        \frac{\partial f}{\partial \theta} \mathbf{e}_\theta +
        \frac{1}{r \sin \theta} \frac{\partial f}{\partial \phi} \mathbf{e}_\phi$$
        $$\begin{split}
        \nabla \times \mathbf{A} & = \frac{1}{r \sin \theta} \left(
        \frac{\partial}{\partial \theta} \left(\mathbf{A}_\phi \sin \theta \right)
        - \frac{\partial \mathbf{A}_\theta}{\partial \phi}
        \right) \mathbf{e}_r \\
        & + \frac{1}{r} \left(
        \frac{1}{\sin \theta} \frac{\partial \mathbf{A}_r}{\partial \phi} -
        \frac{\partial}{\partial r}\left(r \mathbf{A}_\phi \right)
        \right) \mathbf{e}_\theta \\
        & + \frac{1}{r} \left(
        \frac{\partial}{\partial r} \left(r \mathbf{A}_\theta\right) -
        \frac{\partial \mathbf{A}_r}{\partial \theta}
        \right) \mathbf{e}_\phi
        \end{split}$$ Here, $\mathbf{e}_{r, \theta, \phi}$ are the
        local, orthogonal unit vectors for spherical coordinates.
